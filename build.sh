#!/usr/bin/env bash

# Usage:
# build <platform> <config> <system>

# Exit as failure if any command fails
set -e

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
PLATFORM="$1"
CONFIG="$2"
SYSTEM="$3"

# change '/c/...' to 'c:/...' for windows paths
if [[ "${SYSTEM}" == "Windows" ]]; then
    BASE_DIR=`cygpath --windows --absolute ${BASE_DIR}`

    GENERATOR="Visual Studio 16 2019"

    if [[ "${PLATFORM}" == "x64" ]]; then
        CEF_PATH="cef_binary_windows64_minimal"
        BUILD_OPTIONS="-A ${PLATFORM}"
    elif [[ "${PLATFORM}" == "x86" ]]; then
        CEF_PATH="cef_binary_windows32_minimal"
        BUILD_OPTIONS="-A Win32"
    fi

    BUILD_DIR="${BASE_DIR}/${SYSTEM}/${PLATFORM}"
    CMAKE_FLAGS="-DCEF_RUNTIME_LIBRARY_FLAG=//MD -DCEF_DEBUG_INFO_FLAG=//Z7"
elif [[ ("${SYSTEM}" == "CentOS" || "${SYSTEM}" == "Ubuntu") && "${PLATFORM}" == "x64" ]]; then
    GENERATOR="Unix Makefiles"
    CEF_PATH="cef_binary_linux64_minimal"

    BUILD_DIR="${BASE_DIR}/${SYSTEM}/${CONFIG}"
    CMAKE_FLAGS="-DCMAKE_CXX_FLAGS=-fpic"
else
    echo "Incorrect platform and/or system"
    exit 1
fi

mkdir -p "${BUILD_DIR}"
pushd "${BUILD_DIR}"
    cmake -G "${GENERATOR}" ${BUILD_OPTIONS} ${CMAKE_FLAGS} -DCMAKE_BUILD_TYPE=${CONFIG} -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_STANDARD_REQUIRED=TRUE -DCMAKE_CXX_EXTENSIONS=FALSE -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE -DCMAKE_VERBOSE_MAKEFILE=1 -DUSE_SANDBOX=0 "${BASE_DIR}/source/${CEF_PATH}"

    cmake --build . --config ${CONFIG}
popd
