cmake_minimum_required(VERSION 3.0)

set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

if (UNIX)
    find_program(LSB_RELEASE_EXEC lsb_release)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} -d
        OUTPUT_VARIABLE LSB_RELEASE_DESC
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (LSB_RELEASE_DESC MATCHES "Ubuntu")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu18_04/gcc7_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_DESC MATCHES "CentOS")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/centos7/gcc7_3_1/${ARCH_DIR}")
    endif ()

elseif (WIN32)
    set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/windows/vc142/${ARCH_DIR}")
endif ()

add_custom_target(Cef-Resources)
set_target_properties(Cef-Resources PROPERTIES IMPORTED_LOCATION "${PLATFORM_DIR}")


function(add_shared_lib DIR LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")

    add_library("${LIB}" SHARED IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${DIR}")

    if (UNIX)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION "${DIR}/resources/${LIB}${CMAKE_SHARED_LIBRARY_SUFFIX}")
    elseif (WIN32)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_IMPLIB "${DIR}/resources/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION "${DIR}/resources/${LIB}${CMAKE_SHARED_LIBRARY_SUFFIX}")
    endif ()
endfunction(add_shared_lib)


function(add_static_lib STATIC_LIB_DIR LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")

    add_library("${LIB}" STATIC IMPORTED GLOBAL)

    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${STATIC_LIB_DIR}/release/${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${STATIC_LIB_DIR}/debug/${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
endfunction(add_static_lib)


function(copy_cef_resources TARGET_NAME)

    get_target_property(CEF_RESOURCES_LOC Cef-Resources IMPORTED_LOCATION)

    file(GLOB CEF_RESOURCES "${CEF_RESOURCES_LOC}/resources/**")
    foreach (COPY_TARGET ${CEF_RESOURCES})
        if (IS_DIRECTORY ${COPY_TARGET})
                get_filename_component(DIRECTORY_NAME "${COPY_TARGET}" NAME)
                add_custom_command(TARGET ${TARGET_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${COPY_TARGET} "$<TARGET_FILE_DIR:${TARGET_NAME}>/${DIRECTORY_NAME}")
        else ()
                add_custom_command(TARGET ${TARGET_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different ${COPY_TARGET} "$<TARGET_FILE_DIR:${TARGET_NAME}>")
        endif ()
    endforeach ()

endfunction(copy_cef_resources)


function(install_cef_resources COPY_PATH)

    get_target_property(CEF_RESOURCES_LOC Cef-Resources IMPORTED_LOCATION)
    install(DIRECTORY "${CEF_RESOURCES_LOC}/resources/" DESTINATION ${COPY_PATH})

endfunction(install_cef_resources)


add_shared_lib("${PLATFORM_DIR}" "libcef")
add_static_lib("${PLATFORM_DIR}" "libcef_dll_wrapper")
